# DataTable JQuery Plugin #

DataTable Plugin is a table creation plugin for JQuery Library, with support of resizing columns, resonsive layout, fixed columns or rows without loosing performance. This plugin uses <div> tags for creating light weight table with maximum functionality.

### Main goals ###

* Performance
* Light weight
* Responsive layout
* Fixed header and first column


### Features ###

* Resizable columns
* Server side data source
* Fixed header
* Fixed left column
* Lazy load
* Customizable with configuration array
* HTML support in columns content


### Setup ###

Include main CSS file in <head> tag

```
<link rel="stylesheet" type="text/css" href="css/fixedtable.css">
```

Include main JS file in the end of <body> tag, after jQuery Library

```
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="js/fixedtable.js" type="text/javascript"></script>
```

Create <div> tag with some ID , in your HTML code.

```
<div id="ft1"></div>
```

Create DataTable object on <div> with some custom configuration.
Example:

```
var ft11;

jQuery( document ).ready( function () {

    ft11 = jQuery( "#ft1" ).ft( {
        fixedHead: true,
        fixedFCol: true,
        ajaxUrl: './json.php',
        pagesize: 100,
        colWidth: 250,
        sort_by: 'delivery',
        sort_desc: 1,
        minColWidth: 80,
        columns: [
            {
                name: 'c1',
                label: 'H 1',
                drawCol: function ( item, rowItem, row, col, colName ) {
                    return 'Some <b>html</b> <i>tags</i> added to content ' + item;
                },
                width: 50
            },
            {
                name: 'c2',
                label: 'H 2',
                drawCol: function ( item, rowItem, row, col, colName ) {
                    return '<div style="font-weight:bold">' + item + '</div>';
                },
                width: 350
            },
            {
                name: 'c3',
                label: 'H 3'
            },
            {
                name: 'c4',
                label: 'H 4'
            }
        ],
        filterSendData: function ( data ) {

            data.item_type  = 'ads';
            data.fields     = 'delivery,spend,reach,clicks';
            data.adaccounts = [ 123123123, 234234234234, 456456456465 ];
            data.date       = {
                from: '2018-01-01',
                to: '2018-02-01'
            };

            return data;

        }
    } );


} );
```