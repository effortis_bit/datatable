(function ( $ ) {

	var positionStickySupport = function () {
		var el = document.createElement( 'a' );

		mStyle         = el.style;
		mStyle.cssText = 'position:sticky;position:-webkit-sticky;position:-ms-sticky;';

		return mStyle.position.indexOf( 'sticky' ) === -1;
	}();

	$.fn.ft = function ( options ) {

		return new ftTable( this, options );

	};

	var ftTable = function ( _this, options ) {

		var that = this;

		this.xhr;
		this.dragging = -1;
		this.dragPosition;
		this.element  = _this;
		this.total    = -1;
		this.defaults = {
			// Default settings
			fixedHead: false,
			fixedFCol: false,
			columns: [],
			ajaxUrl: '',
			pagesize: 20,
			colWidth: 150,
			filterSendData: function ( data ) {
				return data;
			},
			sort_by: '',
			sort_desc: 1,
			sorting: true,
			minColWidth: 50,
			emptyCallback: null,
			drawCallback: function () {
			}
		};


		var defaults = this.settings || this.defaults;

		this.start = 0;

		this.settings = $.extend( defaults, options );

		$( this.element ).addClass( "ft" );

		$( this.element ).append( '<div class="ftScrollable"></div>' );

		this.elementScrollable = $( this.element ).find( ".ftScrollable" );

		$( this.elementScrollable ).append( '<div class="ftHead"></div>' );

		var tableWidth = 0;
		var len        = this.settings.columns.length;

		for ( var i = 0; i < len; i++ ) {

			var colWidth = that.settings.columns[ i ][ 'width' ] || that.settings.colWidth;

			tableWidth += colWidth;

			if ( i == 1 ) {
				var fColWidth = that.settings.columns[ 0 ][ 'width' ] || that.settings.colWidth;
			}

			var className = this.settings.sorting ? 'ftSort' : '';
			var fieldName = '';
			var name      = this.settings.columns[ i ].name;

			if ( i === 0 ) className += ' fCol';
			if ( typeof name === 'string' ) fieldName = name;
			if ( name && typeof name === 'object' ) fieldName = name[ 0 ];
			if ( that.settings.sort_by === fieldName ) className += ' sorted-by ' + ( that.settings.sort_desc ? 'desc' : 'asc' );

			$( this.elementScrollable ).find( ".ftHead" ).append( '<div style="width: ' + colWidth + 'px;" class="' + className + '" data-sort="' + this.settings.columns[ i ].name + '">' + this.settings.columns[ i ].label + ( i < len - 1 ? '<div class="ftResize"></div>' : '' ) + '</div>' );
		}

		$( this.elementScrollable ).find( ".ftHead" ).data( 'table-width', tableWidth );

		$( this.elementScrollable ).append( '<div class="ftBody"></div>' );

		$( this.elementScrollable ).find( ".ftHead, .ftBody" ).width( tableWidth );


		if ( this.settings.fixedHead ) {
			$( this.element ).addClass( "ftFixedHead" );
		}

		if ( this.settings.fixedFCol ) {
			$( this.element ).addClass( "ftFixedFCol" );
		}

		if ( this.settings.fixedFCol ) {
			var $ftBodyFCols = $( this.element ).find( ".fCol" );
		}

		if ( this.settings.fixedHead ) {
			var $ftHeadContainer = $( this.element ).find( ".ftHead" );
		}

		var lastScrollLeft = 0;
		var lastScrollTop  = 0;

		$( this.elementScrollable ).on( 'scroll', function ( e ) {

			var scrTop         = this.scrollTop;
			var scrLeft        = this.scrollLeft;
			var innerHeightVal = $( that.element )[ 0 ].offsetHeight;

			if ( scrLeft >= 10 ) {
				$( that.element ).addClass( "ftScrolledRight" );
			} else {
				$( that.element ).removeClass( "ftScrolledRight" );
			}

			if ( positionStickySupport ) {

				if ( that.settings.fixedFCol && scrLeft !== lastScrollLeft ) {
					$( this ).find( '.fCol' ).css( 'transform', 'translateX(' + scrLeft + 'px)' );
					lastScrollLeft = scrLeft;
				}

				if ( that.settings.fixedHead && scrTop !== lastScrollTop ) {
					$ftHeadContainer.css( 'transform', 'translateY(' + scrTop + 'px)' );
					lastScrollTop = scrTop;
				}

			}

			if ( scrTop + innerHeightVal >= this.scrollHeight ) {
				getRows();
			}

		} );
		$( this.element ).find( ".ftResize" ).on( 'mousedown', function ( e ) {

			that.dragging = $( this ).parent().index();

			that.dragPosition = { x: e.pageX, y: e.pageY };

			$( that.elementScrollable ).css( "overflow", "hidden" );
			$( that.elementScrollable ).addClass( 'noselect' );

			that.contWidth = $( that.element ).find( '.ftHead' ).data( 'table-width' );

			var index = that.dragging + 1 < that.settings.columns.length ? that.dragging + 1 : that.dragging;

			that.nextColWidth = that.settings.columns[ index ][ 'width' ] || that.settings.colWidth;

		} );

		$( document ).on( 'mousemove', function ( e ) {

			if ( that.dragging === -1 )
				return false;

			var elCont      = $( that.element );
			var delta       = e.pageX - that.dragPosition.x;
			var col         = that.dragging;
			var colWidth    = that.settings.columns[ col ][ 'width' ] || that.settings.colWidth;
			var colMinWidth = that.settings.columns[ col ][ 'minWidth' ] || null;
			var colMaxWidth = that.settings.columns[ col ][ 'maxWidth' ] || null;
			var newWidth    = colWidth + delta;
			var minColWidth = colMinWidth ? colMinWidth : that.settings.minColWidth;
			var tableWidth  = elCont.find( '.ftScrollable' ).width();
			var contWidth   = that.contWidth + delta;

			if ( newWidth < minColWidth ) newWidth = minColWidth;
			if ( colMaxWidth && newWidth > tableWidth - 30 ) newWidth = tableWidth - 30;

			var nw = that.nextColWidth + ( colWidth - newWidth );

			if ( contWidth < tableWidth ) {
				contWidth = tableWidth;

				if ( that.settings.columns[ col + 1 ] ) {
					that.settings.columns[ col + 1 ][ 'width' ] = nw;
					elCont.find( ".ftHead>div:nth-child(" + (col + 2) + ")" ).css( "width", nw + "px" );
					elCont.find( ".ftBody .ftRow>div:nth-child(" + (col + 2) + ")" ).css( "width", nw + "px" );
				}
			}

			var $w = 0;

			$.each( elCont.find( '.ftHead > div' ), function ( i, v ) {
				$w += v.offsetWidth;
			} );

			elCont.find( ".ftHead>div:nth-child(" + (col + 1) + ")" ).css( "width", newWidth + "px" );
			elCont.find( ".ftBody .ftRow>div:nth-child(" + (col + 1) + ")" ).css( "width", newWidth + "px" );
			elCont.find( ".ftHead, .ftBody" ).width( $w );

		} );

		$( document ).on( 'mouseup', function ( e ) {

			e.preventDefault();

			if ( that.dragging === -1 ) {

				var self = $( e.target ).hasClass( 'ftSort' ) ? $( e.target ) : $( e.target ).closest( '.ftSort' );
				sortRows( self );

				return false;
			}

			$( that.elementScrollable ).css( "overflow", "auto" );
			$( that.elementScrollable ).removeClass( 'noselect' );

			var elCont      = $( that.element );
			var col         = that.dragging;
			var delta       = e.pageX - that.dragPosition.x;
			var colWidth    = that.settings.columns[ col ][ 'width' ] || that.settings.colWidth;
			var colMinWidth = that.settings.columns[ col ][ 'minWidth' ] || null;
			var colMaxWidth = that.settings.columns[ col ][ 'maxWidth' ] || null;
			var newWidth    = colWidth + delta;
			var minColWidth = colMinWidth ? colMinWidth : that.settings.minColWidth;
			var tableWidth  = elCont.find( '.ftScrollable' ).width();
			var $cont       = elCont.find( '.ftHead' );

			if ( newWidth < minColWidth ) newWidth = minColWidth;
			if ( colMaxWidth && newWidth > tableWidth - 30 ) newWidth = tableWidth - 30;

			that.settings.columns[ col ][ 'width' ] = newWidth;

			$cont.data( 'table-width', $cont.width() );

			that.dragging     = -1;
			that.dragPosition = null;

		} );

		var sortRows = function ( self ) {

			var sort_by   = 'desc';
			var sort_desc = 1;

			if ( self.data( 'sort' ) === null ) return false;

			if ( self.hasClass( 'sorted-by' ) ) {

				if ( self.hasClass( 'desc' ) ) {

					self.removeClass( 'desc' ).addClass( 'asc' );
					sort_by   = self.data( 'sort' );
					sort_desc = 0;

				} else if ( self.hasClass( 'asc' ) ) {

					self.removeClass( 'asc' ).addClass( 'desc' );
					sort_by   = self.data( 'sort' );
					sort_desc = 1;
				}

			} else {

				$( that.element ).find( '.ftSort' ).removeClass( 'sorted-by desc asc' );
				self.addClass( 'sorted-by desc' );
				sort_by   = self.data( 'sort' );
				sort_desc = 1;

			}

			$( that.element ).find( ".ftBody" ).empty();

			getRows( {
				sort_by: sort_by,
				sort_desc: sort_desc,
				start: 0
			} );

		};

		var gettingRows = false;

		var getRows = function ( params ) {

			if ( gettingRows || ( that.total != -1 && ( params ? params.start : that.start ) >= that.total ) )
				return false;

			gettingRows = true;

			var data = that.settings.filterSendData( {
				pagesize: that.settings.pagesize,
				start: params ? params.start : that.start,
				sort_by: params ? params.sort_by : that.settings.sort_by,
				sort_desc: params ? params.sort_desc : that.settings.sort_desc
			} );

			that.xhr = $.ajax( {
				url: that.settings.ajaxUrl,
				dataType: 'json',
				data: data,
				beforeSend: function ( xhr ) {
					showLoading();
				}
			} )
			.done( function ( data ) {

				if ( !data || !data.items ) return false;

				that.total = data.total;

				drawRows( data );

				that.start += that.settings.pagesize;

				hideLoading();

				gettingRows = false;

			} );

		};

		var drawRows = function ( data ) {

			if ( data.items.length == 0 && data.total == 0 ) {

				if ( that.settings.emptyCallback && typeof that.settings.emptyCallback === 'function' )
					that.settings.emptyCallback( $( that.element ) );
				else
					emptyMessage();

			} else {

				for ( var i = 0; i < data.items.length; i++ ) {

					drawRow( data.items[ i ], that.start + i );

				}

			}

			that.settings.drawCallback( $( that.element ), data.items, data.total, data );

		};

		var drawRow = function ( item, row ) {

			var rowHTML = '<div class="ftRow">';

			for ( var i = 0; i < that.settings.columns.length; i++ ) {

				rowHTML += drawCol( item[ that.settings.columns[ i ].name ], item, row, i, that.settings.columns[ i ].name );

			}

			rowHTML += '</div>';

			$( that.element ).find( ".ftBody" ).append( rowHTML );

		};

		var drawCol = function ( item, rowItem, row, col, colName ) {

			var colWidth = that.settings.columns[ col ][ 'width' ] || that.settings.colWidth;

			if ( col == 1 ) {
				var fColWidth = that.settings.columns[ 0 ][ 'width' ] || that.settings.colWidth;
			}

			var colHTML = '<div style="width: ' + colWidth + 'px;" ' + ( col == 0 ? 'class="fCol"' : '' ) + '>';

			colHTML += that.settings.columns[ col ][ 'drawCol' ] ? that.settings.columns[ col ][ 'drawCol' ]( item, rowItem, row, col, colName ) : item;

			colHTML += '</div>';

			return colHTML;

		};

		var emptyMessage = function () {

			$( that.element ).find( ".ftScrollable" ).append( '<div class="ftRow ftCenterMsg">Nothing found</div>' );

		};

		var showLoading = function () {

			$( that.element ).find( ".ftScrollable" ).append( '<div class="ftRow ftCenterMsg ftLoading">Loading ...</div>' );

		};

		var hideLoading = function () {

			$( that.element ).find( ".ftScrollable" ).find( ".ftLoading" ).remove();

		};

		getRows();

		this.reloadContent = function () {

			if ( this.xhr )
				this.xhr.abort();

			gettingRows = false;

			$( this.element ).find( ".ftBody" ).html( '' );

			this.start = 0;
			this.total = -1;

			getRows();

		};

		this.destroy = function () {

			if ( this.xhr )
				this.xhr.abort();

			gettingRows = false;

			$( this.element ).html( '' );
			$( this.element ).removeClass( 'ft' ).removeClass( 'ftFixedHead' ).removeClass( 'ftFixedFCol' );

			this.start        = 0;
			this.total        = -1;
			this.settings     = this.defaults;
			this.element      = null;
			this.dragging     = -1;
			this.dragPosition = null;

			return null;

		};

	};

}( jQuery ));