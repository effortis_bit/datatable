var ft11;

$( document ).ready( function () {

    ft11 = jQuery( "#ft1" ).ft( {
        fixedHead: true,
        fixedFCol: true,
        ajaxUrl: './json.php',
        pagesize: 100,
        colWidth: 250,
        sort_by: 'delivery',
        sort_desc: 1,
        minColWidth: 80,
        columns: [
            {
                name: 'c1',
                label: 'H 1',
                drawCol: function ( item, rowItem, row, col, colName ) {
                    return item + ' <b>Add some html to first column</b> <a href="#">here</a>';
                },
                width: 400,
                minWidth: 200,
                maxWidth: true
            },
            {
                name: 'c2',
                label: 'H 2',
                drawCol: function ( item, rowItem, row, col, colName ) {
                    return 'Some long text here, verlong text, very very very long text';
                }
            },
            {
                name: 'c3',
                label: 'H 3'
            },
            {
                name: 'c4',
                label: 'H 4'
            },
            {
                name: 'c5',
                label: 'H 5'
            },
            {
                name: 'c6',
                label: 'H 6'
            },
            {
                name: 'c7',
                label: 'H 7'
            },
            {
                name: 'c8',
                label: 'H 8'
            },
            {
                name: 'c9',
                label: 'H 9'
            },
            {
                name: 'c10',
                label: 'H 10'
            }
        ],
        filterSendData: function ( data ) {

            data.item_type  = 'ads';
            data.fields     = 'delivery,spend,reach,clicks';
            data.adaccounts = [ 123123123, 234234234234, 456456456465 ];
            data.date       = {
                from: '2018-01-01',
                to: '2018-02-01'
            };

            return data;

        }
    } );

} );