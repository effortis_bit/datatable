<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <script src="./js/fixedtable.js?r=<?= rand(0,100000) ?>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="./css/fixedtable.css?r=<?= rand(0,100000) ?>">

    <script src="./js/script.js?r=<?= rand(0,100000) ?>" type="text/javascript"></script>

</head>
<body>
<ul>
<li>Resizable columns</li>
<li>Server side data source</li>
<li>Fixed header</li>
<li>Fixed left column</li>
<li>Lazy load</li>
<li>Customizable with configuration array</li>
<li>HTML support in columns content</li>
</ul>
<br>
<b>There is a 2 second delay from server side for demonstrating lazy load. Demo data has 500 rows.</b>
<br>
<br>

<div id="ft1"></div>



</body>
</html>